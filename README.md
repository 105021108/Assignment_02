# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
1. 有完整的遊戲流程(有loading、menu、game_view、game_over、quit_game or restart_game)
2. 基本規則跟原版小朋友下樓梯相同(不能跳、只能控制左右移動、自由落體時亦可控制左右移動、有生命值、碰到尖刺會扣血、踩其他種bar會補血、成績以樓層計)
3. 都有正確的物理性質(除了往上彈時不會撞到上層的樓梯，這是為了讓玩家移動方便)
4. 我有做基本的 一般平台、翻轉平台、輸送帶(有向左送也有向右送)、彈簧、尖刺，另外還有做一種新的陷阱平台
5. 有星際大戰的背景音樂、經典的慘死聲
6. 我有做自己的成績排行(前五)和前五名的成績
7. 我的圖片都是拿原本就有的圖片啦哈哈，不然就是自己拿原本的來改，總覺得這樣風格比較一致
8. 想不太到可以做什麼，就 多做一種陷阱平台(若站太中間會陷入陷阱，然後被陷阱往下射);還有 難度會隨著樓層增加而增加，具體來說就是 一般平台出現機率降低 & 其他種樓梯出現機率增加 & 樓梯上升速度加快 & 樓梯平均出現頻率降低 ，這樣比較有破關的感覺哈哈，不過都是過40階左右才會感覺有點不一樣啦; 另外我也有作保底措施，基本上有太大一段距離沒出平台就會強制讓它出一個，但保底措施出來的平台也是隨機平台哈哈