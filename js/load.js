var loadState = { 
    preload: function () {
        // Add a 'loading...' label on the screen 
        var loadingLabel = game.add.text(game.width/2, 90+150, 'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar 
        var progressBar = game.add.sprite(game.width/2, 90+200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);
        // Load all game assets 
        game.load.image('background', 'assets/bg.png');
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.image('bar', 'assets/bar.png');
        game.load.image('wall', 'assets/wall.png');
        game.load.spritesheet('spring', 'assets/spring.png', 97, 22);
        game.load.image('thorn', 'assets/thorn.png');
        game.load.spritesheet('fake', 'assets/fake.png', 97, 36);
        game.load.spritesheet('trap', 'assets/trap.png', 96, 15);
        game.load.spritesheet('conveyor_left', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('conveyor_right', 'assets/conveyor_right.png', 96, 16);
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.audio('audio1', 'assets/bgm1.ogg');
        game.load.audio('diesound', 'assets/sound.mp3');
        // Load a new asset that we will use in the menu state 
    }, 
    update: function() { 
        // Go to the menu state 
        if(i==n) game.state.start('menu');
    }
};