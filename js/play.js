var velocity = 175;
var jumppower = 300;
var unbeatabletime =500;
var hurtstate = 0;
var hasfly = 0;
var before_health = "health: "
var before_level = "level "
var hurtbyceilingtime = 0;
var num_of_repeat_user = 0;
var num_of_repeat_grade = 0;
var downbar = [];
var playState = {
    create: function(){
        this.bgmMusic = game.add.audio('audio1');
        this.bgmMusic.loop = true;
        this.bgmMusic.play(); 
        this.sound = game.add.audio('diesound');
        game.add.image(16, 0, 'background');
        game.add.image(16, 360, 'background');
        //initialize
        game.global.level = 0;
        hurtbyceilingtime = 0;
        this.bararray = [];

        //player
        this.cursor = game.input.keyboard.createCursorKeys();
        this.playeranimate();
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        this.player.maxHealth = 10;
        this.player.health = this.player.maxHealth;
        //floor
        this.bars = game.add.group();

        //ceiling
        this.ceiling = game.add.sprite(16 , 0, 'ceiling', 0, this.bars); 
        game.physics.arcade.enable(this.ceiling);
        this.ceiling.body.immovable = true;
        this.ceiling.anchor.setTo(0,0.5);

        //first bar
        var bar = game.add.sprite(this.player.x, this.player.y + this.player.height, 'bar', 0, this.bars); 
        game.physics.arcade.enable(bar);
        bar.body.velocity.y = -50*3/2;
        bar.body.immovable = true;
        bar.anchor.setTo(0.5,0);
        this.bararray.push(bar);
        
        //wall
        this.wallleft1 = game.add.sprite(376, 0, 'wall');
        game.physics.arcade.enable(this.wallleft1);
        this.wallleft1.body.immovable = true;
        this.wallright1 = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.wallright1);
        this.wallright1.body.immovable = true;
        this.wallleft2 = game.add.sprite(376, 350, 'wall');
        game.physics.arcade.enable(this.wallleft2);
        this.wallleft2.body.immovable = true;
        this.wallright2 = game.add.sprite(0, 350, 'wall');
        game.physics.arcade.enable(this.wallright2);
        this.wallright2.body.immovable = true;

        //timer
        this.Timer = 0;
        //text
        this.health = game.add.text(20, 16, before_health + this.player.health,{ font: '20px Arial', fill: '#ffffff' });
        this.leveltext = game.add.text(300, 16, before_level + game.global.level,{ font: '20px Arial', fill: '#ffffff' });
    },
    update: function() {
        downbar = [];
        game.physics.arcade.collide(this.player, this.bars, this.bartouch, this.test, this);
        game.physics.arcade.collide(this.player, this.wallleft1);
        game.physics.arcade.collide(this.player, this.wallright1);
        game.physics.arcade.collide(this.player, this.wallleft2);
        game.physics.arcade.collide(this.player, this.wallright2);

        if( this.player.health <= 0 ) this.player.health=0; 
        if(!this.player.body.touching.down) hasfly = 1;
        this.health.setText(before_health+this.player.health);
        this.leveltext.setText(before_level+game.global.level);

        //build bar
        if(this.Timer%50==0){
            var barTypes = ["spring", "spring", 
                            "thorn", "thorn", "thorn", 
                            "fake", "fake",
                            "trap", "trap",
                            "conveyor_left", "conveyor_right",
                            "bar", "bar", "bar", "bar", "bar"];
            if(Math.floor(game.global.level/20)) barTypes.pop();
            if(Math.floor(game.global.level/40)) barTypes.pop();
            if(Math.floor(game.global.level/60)) barTypes.pop();
            if(Math.floor(game.global.level/80)) barTypes.pop();
            var willdo = Math.floor( Math.random() * (4-game.global.level/75));//機率隨level下降
            if( willdo || this.bararray[this.bararray.length-1].y < game.height/3){
                var x = Math.random() * (game.width-2*16-96)+16;
                var type = barTypes[Math.floor(Math.random() * barTypes.length)];
                var bar = game.add.sprite(x, game.height, type, 0, this.bars);
                game.physics.arcade.enable(bar);
                if( type == "spring" ) {
                    bar.body.setSize(97, 17, 0, 9);
                    bar.frame = 3;
                    bar.animations.add('work', [3, 2, 1, 0, 1, 2, 3, 4, 5, 4, 3],22, false);
                }
                else if( type == "fake" ){
                    bar.body.setSize(97, 17, 0, 9);
                    bar.frame = 0;
                    bar.animations.add('work', [0, 1, 2, 3, 4, 5, 0],14, false);
                }
                else if( type == "thorn" ){
                    bar.body.setSize(96, 15, 0, 15);
                }
                else if( type == "conveyor_left" ){
                    bar.frame = 0;
                    bar.animations.add('work', [0, 1, 2, 3],8, true);
                    bar.animations.play('work');
                }
                else if( type == "conveyor_right" ){
                    bar.frame = 0;
                    bar.animations.add('work', [0, 1, 2, 3],8, true);
                    bar.animations.play('work');
                }
                else if( type == "trap" ){
                    bar.frame = 0;
                    bar.animations.add('work', [0, 1, 2, 3, 4, 4, 4, 3, 2, 1, 0],11, false);
                }
                bar.body.velocity.y = -(50+game.global.level)*3/2;//速度隨level上升
                bar.body.immovable = true;
                this.bararray.push(bar);
                this.player.bringToTop();
                this.ceiling.bringToTop();
            }
            if(this.Timer%500==0) game.global.level++;
        }
        if(this.player.alive || this.Timer%50 == 0) this.Timer++;

        //delete bar
        if(this.bararray.length>0){
            this.bararray.reverse();
            if (!this.bararray[this.bararray.length-1].inWorld) {
                this.bararray[this.bararray.length-1].kill();
                this.bararray.pop();
            }
            this.bararray.reverse();
        }

        //hurtstate
        if( hurtstate == 1 ){
            var hurtTimer = game.time.create(true);
            hurtTimer.add(unbeatabletime, function(){ hurtstate = 0; });
            hurtTimer.start();
            hurtstate = 2;
        }
        
        //player die
        if ( ( !this.player.inWorld || this.player.health == 0 ) && this.player.alive ) this.playerDie(); 
        if(this.player.alive) this.movePlayer();
    }, 
    playeranimate: function(){
        this.player = game.add.sprite(game.width/2, game.height*3/4, 'player');
        this.player.facingLeft = false;
        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8, true);
        this.player.animations.add('leftwalk_damage', [4, 5, 6, 7], 8, true);
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 8, true);
        this.player.animations.add('rightwalk_damage', [13, 14, 15, 16], 8, true);
        this.player.animations.add('stand_damage', [17, 8, 17, 8], 8, true);
        this.player.animations.add('leftjump', [18, 19, 20, 21], 8, true);
        this.player.animations.add('leftjump_damage', [22, 23, 24, 25], 8, true);
        this.player.animations.add('rightjump', [27, 28, 29, 30], 8, true);
        this.player.animations.add('rightjump_damage', [31, 32, 33, 34], 8, true);
        this.player.animations.add('posjump', [36, 37, 38, 39], 8, true);
        this.player.animations.add('posjump_damage', [40, 41, 42, 43], 8, true);
    },
    bartouch: function(player, bar) {
        if(this.player.body.touching.down){//觸發動畫&效果
            downbar = bar.key;
            if( bar.key=="spring" )this.player.body.velocity.y = -jumppower;
            if( bar.key=="spring" || bar.key=="fake" ) bar.animations.play('work');
            if( bar.key=="trap" && Math.abs(player.x + player.width/2 - bar.x - bar.width/2)< bar.width/3 ){
                bar.animations.play('work');
                if(bar.frame==1) {
                    bar.body.setSize(96, 15*3/4, 0, 15*1/4);
                    this.player.body.y = bar.y  + 15*1/4 - player.height;
                }
                if(bar.frame==2) {
                     bar.body.setSize(96, 15*2/4, 0, 15*2/4);
                     this.player.body.y = bar.y + 15*2/4 - player.height;
                }
                if(bar.frame==3) {
                    bar.body.setSize(96, 15*1/4, 0, 15*3/4);
                    this.player.body.y = bar.y + 15*3/4 - player.height;
                }
                if(bar.frame==4) {
                    bar.body.setSize(96, 15, 0, 0);
                    this.player.body.velocity.y = jumppower/1.5;
                }
                this.player.body.x = (2*player.body.x + bar.x + bar.width/2 - player.width/2)/3;
            }
        }
        if( (this.player.body.touching.down && bar.key=="thorn"  && hasfly ) || bar.key=="ceiling" ){
            if( hurtstate == 0 ){
                this.player.health -= 4;
                hurtstate = 1;
                if(bar.key=="thorn") hasfly = 0;
                else hurtbyceilingtime = this.Timer;
            }
        }
        else if(this.player.body.touching.down && hasfly){
            if( this.player.health < this.player.maxHealth && !this.player.body.touching.up && this.Timer - hurtbyceilingtime > 10){
                this.player.health ++;
                hurtstate = 0;
                hasfly = 0;
            }
        }
    },
    test: function(player,bar){
        if( bar.key=="ceiling" || ( player.y + player.height < bar.y + bar.height && player.y > 16 ) ){
            if( bar.key!="fake" ) return true;
            else return (bar.frame < 1);
        }
        else return false;
    },
    playerDie: function() { 
        this.player.kill();
        this.bgmMusic.destroy();
        this.sound.play(); 
        //丟成績
        if( game.global.level > game.global.levels[0] ) {
            var userref = firebase.database().ref("users/" + user_name);
            userref.push().set({
                'level': game.global.level,
            })
            game.global.levels[0] = game.global.level;
            for(i=1;i<5;i++){
                if( game.global.level > game.global.levels[i] ){
                    game.global.levels[i-1] = game.global.levels[i];
                    game.global.levels[i] = game.global.level;
                }
                else break;
            }
            for(i=0;i<4;i++) if(game.global.levels[1+i]!=game.global.levels[0]) break;
            num_of_repeat_user = i;
            userref.orderByChild("level").once("value").then(function(snapshot) {
                var k = 0;
                snapshot.forEach(function(childSnapshot) {
                    if( childSnapshot.val().level < game.global.levels[0] )
                        userref.child(childSnapshot.key).remove();
                    else if( childSnapshot.val().level == game.global.levels[0] ){
                        if( k <= num_of_repeat_user ) k++;
                        else userref.child(childSnapshot.key).remove();
                    }
                });     
            });
        }
        if( game.global.level > game.global.ranking_levels[0] ) {
            var graderef = firebase.database().ref("grade")
            graderef.push().set({
               'user_name': user_name,
               'level': game.global.level,
            })
            game.global.ranking_levels[0] = game.global.level;
            for(i=1;i<5;i++){
                if( game.global.level > game.global.ranking_levels[i] ){
                    game.global.ranking_levels[i-1] = game.global.ranking_levels[i];
                    game.global.ranking_levels[i] = game.global.level;
                }
                else break;
            }
            for(i=0;i<4;i++) if(game.global.ranking_levels[1+i]!=game.global.ranking_levels[0]) break;
            num_of_repeat_grade = i;
            graderef.orderByChild("level").once("value").then(function(snapshot) {
                var k = 0;
                snapshot.forEach(function(childSnapshot) {
                    if( childSnapshot.val().level < game.global.ranking_levels[0] )
                        graderef.child(childSnapshot.key).remove();
                    else if( childSnapshot.val().level == game.global.ranking_levels[0] ){
                        if( k <= num_of_repeat_grade ) k++;
                        else graderef.child(childSnapshot.key).remove();
                    }
                });     
            });
        }

        if( this.player.body.y<game.height ) {
            /// Particle
            this.emitter = game.add.emitter(this.player.body.x + this.player.width/2, 
                                            this.player.body.y + this.player.height/2, 50);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;
            this.emitter.start(true, 1000, null, 50);
        }
        var dieTimer = game.time.create(true);
        dieTimer.add(1000, function() {game.state.start('pass');});
        dieTimer.start();
    },

    movePlayer: function() {
        if( this.cursor.left.isUp ) this.cursor.left.duration = 0;
        if( this.cursor.right.isUp ) this.cursor.right.duration = 0;
        if( this.cursor.left.isDown && this.cursor.right.isDown){
            if( this.cursor.left.duration < this.cursor.right.duration ) this.player.facingLeft = true;
            else this.player.facingLeft = false;
        }
        else if( this.cursor.left.isDown ) this.player.facingLeft = true;
        else if( this.cursor.right.isDown ) this.player.facingLeft = false;

        if(this.player.facingLeft){
            this.player.body.velocity.x = -velocity;
            if(this.player.body.touching.down){
                if(hurtstate) this.player.animations.play('leftwalk_damage');
                else this.player.animations.play('leftwalk');
            }
            else{
                if(hurtstate) this.player.animations.play('leftjump_damage');
                else this.player.animations.play('leftjump');
            }
        }
        else{
            this.player.body.velocity.x = velocity;
            if(this.player.body.touching.down){
                if(hurtstate) this.player.animations.play('rightwalk_damage');
                else this.player.animations.play('rightwalk');
            }
            else{
                if(hurtstate) this.player.animations.play('rightjump_damage');
                else this.player.animations.play('rightjump');
            }
        }
        if ( !this.cursor.left.isDown && !this.cursor.right.isDown ){
            this.player.body.velocity.x = 0;
            if(this.player.body.touching.down){
                if(hurtstate) this.player.animations.play('stand_damage');
                else {
                    this.player.frame = 8;
                    this.player.animations.stop();
                }
            }
            else{
                if(hurtstate) this.player.animations.play('posjump_damage');
                else this.player.animations.play('posjump');
            }
        }  
        this.player.body.velocity.x += (downbar=="conveyor_left")? -velocity/2 :
                                            (downbar=="conveyor_right")? velocity/2 : 0 ;
    }
};
