var menuState = {
    create: function(){
    // Display the name of the game 
    var nameLabel = game.add.text(game.width/2, 90+80, '小朋友下樓梯', { font: '30px Arial', fill: '#ffffff' }); 
    nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen 
    var scoreLabel = game.add.text(game.width/2, game.height/2, 'highest level: ' + game.global.levels[4], { font: '25px Arial', fill: '#ffffff' });
    scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game 
    var startLabel = game.add.text(game.width/2, game.height-90-100, 'press up key to start game', { font: '25px Arial', fill: '#ffffff' }); 
    startLabel.anchor.setTo(0.5, 0.5); 
    var gradeLabel = game.add.text(game.width/2, game.height-90-60, 'press down key to see grade', { font: '25px Arial', fill: '#ffffff' }); 
    gradeLabel.anchor.setTo(0.5, 0.5); 
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this); 
    var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    downKey.onDown.add(this.seerank, this); 
    }, 
    start: function() { 
        game.state.start('play'); 
    }, 
    seerank: function(){
        game.state.start('selfranking'); 
    }
};