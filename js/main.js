var game;
var user_name;
var i,j,n;
function getname(){
    var txtname = document.getElementById('name');
    if(!txtname.value.match(/^([0-9]|[a-z])+([0-9a-z]+)$/i)) alert("please enter your name");
    else{
        user_name = txtname.value;
        document.getElementById('canvas').innerHTML = "";
        // Initialize Phaser 
        game = new Phaser.Game(392, 355*3/2, Phaser.AUTO, 'canvas');
        // Define our global variable
        game.global = { level: 0 , levels: [0,0,0,0,0] , ranking_levels: [0,0,0,0,0]}; 

        j = 0;
        firebase.database().ref("grade").orderByChild("level").once("value").then(function(snapshot) {
            var m = snapshot.numChildren();
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                game.global.ranking_levels[5-m+j] = childData.level ;
                j++;
            });
        });
        i = 0;
        firebase.database().ref("users/" + user_name).orderByChild("level").once("value").then(function(snapshot) {
            n = snapshot.numChildren();
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                game.global.levels[5-n+i] = childData.level ;
                i++;
            });
            // Add all the states 
            game.state.add('boot', bootState); 
            game.state.add('load', loadState); 
            game.state.add('menu', menuState); 
            game.state.add('play', playState); 
            game.state.add('pass', passState); 
            game.state.add('selfranking', selfrankingState); 
            game.state.add('gameover', gameoverState); 
            // Start the 'boot' state 
            game.state.start('boot');
        });
    }
}
