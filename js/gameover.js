var ranking = [];
var gameoverState = {
    create: function(){
        var levelLabel = game.add.text(game.width/2, 90+80, 'Gameover, level: ' + game.global.level, 
                                       { font: '25px Arial', fill: '#ffffff' }); 
        levelLabel.anchor.setTo(0.5, 0.5);

        i=0;
        firebase.database().ref("grade").orderByChild("level").limitToLast(5).once("value").then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                ranking.push(game.add.text(game.width/2, 90+90+30*(5-i), childData.user_name + ': ' + childData.level,
                                            { font: '25px Arial', fill: '#ffffff' }));
                ranking[i].anchor.setTo(0.5, 0.5);
                i++;
            });
        });

        var startLabel = game.add.text(game.width/2, game.height-90-80, 'press down key to back', { font: '25px Arial', fill: '#ffffff' }); 
        startLabel.anchor.setTo(0.5, 0.5); 
        var quitLabel = game.add.text(game.width/2, game.height-90-40, 'press "q" to quit', { font: '25px Arial', fill: '#ffffff' }); 
        quitLabel.anchor.setTo(0.5, 0.5); 

        var downkey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        downkey.onDown.add(this.start, this); 
        var qkey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        qkey.onDown.add(this.quit, this); 
    }, 
    start: function() { 
        for(i=0;i<ranking.length;i++) ranking[i].destroy();
        ranking = [];
        game.state.start('menu'); 
    }, 
    quit: function(){
        for(i=0;i<ranking.length;i++) ranking[i].destroy();
        ranking = [];
        game.destroy();
        document.getElementById("canvas").innerHTML = 
            "<p class='setting-form'>"+
                "<a>What's your name?(限英文&數字)</a>"+
                "<input type='text' id='name' class='form-control' placeholder='' autofocus>"+
                "<button class='btn btn-lg btn-primary btn-block' onclick='getname()'>sent</button>"+
            "</p>";
    }
}