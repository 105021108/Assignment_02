var selfranking = [];
var num_of_grade;
var selfrankingState = {
    create: function(){
        num_of_grade = 0;
        var graderef = firebase.database().ref("users/" + user_name);
        graderef.orderByChild("level").limitToLast(5).once("value").then( function(snapshot) {
            num_of_grade = snapshot.numChildren();
            i=0;
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val().level;
                selfranking.push(game.add.text(game.width/2, 90+90+30*(num_of_grade-i), user_name + ': ' + childData,
                                            { font: '25px Arial', fill: '#ffffff' }));
                selfranking[i].anchor.setTo(0.5, 0.5);
                i++;
            });
        });

        var startLabel = game.add.text(game.width/2, game.height-90-80, 'press down key to back', { font: '25px Arial', fill: '#ffffff' }); 
        startLabel.anchor.setTo(0.5, 0.5); 

        var downkey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        downkey.onDown.add(this.start, this); 
    }, 
    start: function() { 
        for(i=0;i<selfranking.length;i++) selfranking[i].destroy();
        selfranking = [];
        game.state.start('menu'); 
    }, 
}